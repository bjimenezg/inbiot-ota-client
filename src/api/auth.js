import request from 'utils/request'

export function loadUser () {
  return request('users/me', {
    method: 'GET'
  }).then(([ body ]) => body)
}

export function login (data) {
  return request('auth', {
    method: 'POST',
    data
  }).then(([ body ]) => body)
}
