import request from 'utils/request'

export function loadOta () {
  return request('otas', {
    method: 'GET'
  }).then(([ body ]) => body)
}

export function updateOta (data) {
  return request('otas', {
    method: 'PUT',
    data: data
  }).then(([ body ]) => body)
}
