import request from 'utils/request'

export function loadSystems () {
  return request('systems', {
    method: 'GET'
  }).then(([ body ]) => body)
}
