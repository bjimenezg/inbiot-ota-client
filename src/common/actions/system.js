import { createAction, createRequestTypes, createRequestAction } from 'utils'

export const LOAD_SYSTEMS = '@system/LOAD_SYSTEMS'
export const LOAD_SYSTEM = '@system/LOAD_SYSTEM'
export const CREATE_SYSTEM = '@system/CREATE_SYSTEM'
export const UPDATE_SYSTEM = '@system/UPDATE_SYSTEM'
export const DELETE_SYSTEM = '@system/DELETE_SYSTEM'

export const LOAD_SYSTEMS_REQUEST = createRequestTypes('system/LOAD_SYSTEMS_REQUEST')
export const LOAD_SYSTEM_REQUEST = createRequestTypes('system/LOAD_SYSTEM_REQUEST')
export const CREATE_SYSTEM_REQUEST = createRequestTypes('system/CREATE_SYSTEM_REQUEST')
export const UPDATE_SYSTEM_REQUEST = createRequestTypes('system/UPDATE_SYSTEM_REQUEST')
export const DELETE_SYSTEM_REQUEST = createRequestTypes('system/DELETE_SYSTEM_REQUEST')

export const constants = {
  LOAD_SYSTEMS,
  LOAD_SYSTEM,
  CREATE_SYSTEM,
  UPDATE_SYSTEM,
  DELETE_SYSTEM,

  LOAD_SYSTEMS_REQUEST,
  LOAD_SYSTEM_REQUEST,
  CREATE_SYSTEM_REQUEST,
  UPDATE_SYSTEM_REQUEST,
  DELETE_SYSTEM_REQUEST
}

export const loadSystems = createAction(LOAD_SYSTEMS)
export const loadSystem = createAction(LOAD_SYSTEM)
export const createSystem = createAction(CREATE_SYSTEM)
export const updateSystem = createAction(UPDATE_SYSTEM)
export const deleteSystem = createAction(DELETE_SYSTEM)

export const loadSystemsRequest = createRequestAction(LOAD_SYSTEMS_REQUEST)
export const loadSystemRequest = createRequestAction(LOAD_SYSTEM_REQUEST)
export const createSystemRequest = createRequestAction(CREATE_SYSTEM_REQUEST)
export const updateSystemRequest = createRequestAction(UPDATE_SYSTEM_REQUEST)
export const deleteSystemRequest = createRequestAction(DELETE_SYSTEM_REQUEST)

export default {
  loadSystems,
  loadSystem,
  createSystem,
  updateSystem,
  deleteSystem,

  loadSystemsRequest,
  loadSystemRequest,
  createSystemRequest,
  updateSystemRequest,
  deleteSystemRequest
}
