import { createAction, createRequestTypes, createRequestAction } from 'utils'

export const LOAD_USERS = '@user/LOAD_USERS'

export const LOAD_USERS_REQUEST = createRequestTypes('user/LOAD_USERS_REQUEST')

export const constants = {
  LOAD_USERS,

  LOAD_USERS_REQUEST
}

export const loadUsers = createAction(LOAD_USERS)

export const loadUsersRequest = createRequestAction(LOAD_USERS_REQUEST)

export default {
  loadUsers,

  loadUsersRequest
}
