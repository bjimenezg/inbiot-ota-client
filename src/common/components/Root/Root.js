import React from 'react'
import PropTypes from 'prop-types'
import { Provider } from 'react-redux'
import { applyRouterMiddleware, Router } from 'react-router'
import createRoutes from 'modules/router'

const Root = ({ store, history }) => (
  <Provider store={store}>
    <Router
      render={applyRouterMiddleware()}
      history={history}
      routes={createRoutes(store)} />
  </Provider>
)

Root.propTypes = {
  history: PropTypes.object.isRequired,
  store: PropTypes.object.isRequired
}

export default Root
