import { fromJS } from 'immutable'
//Importando archivos//
import { createReducer } from 'utils'
import { AUTH_USER_REQUEST,  LOGIN_REQUEST,  LOGOUT} from 'common/actions/auth'
import {LOAD_SYSTEMS_REQUEST,CREATE_SYSTEM_REQUEST,UPDATE_SYSTEM_REQUEST,DELETE_SYSTEM_REQUEST} from 'common/actions/system'
import {LOAD_USERS_REQUEST} from 'common/actions/user'

export const initialState = fromJS({ //definimos el estado inicial
  users: {},
  systems: {}
})

const mergeEntities = (state, { payload }) => {
  return state.withMutations(state =>
    Object.keys(payload.entities).reduce(
      (_state, entity) => _state.mergeDeepIn([entity], payload.entities[entity]),
      state
    )
  )
}

export default createReducer(initialState, {
  // User actions
  [LOGOUT]: () => initialState,
  [AUTH_USER_REQUEST.SUCCESS]: mergeEntities,
  [LOGIN_REQUEST.SUCCESS]: mergeEntities,

  // System actions
  [LOAD_SYSTEMS_REQUEST.SUCCESS]: mergeEntities,
  [CREATE_SYSTEM_REQUEST.SUCCESS]: mergeEntities,
  [UPDATE_SYSTEM_REQUEST.SUCCESS]: mergeEntities,
  [DELETE_SYSTEM_REQUEST.SUCCESS]: (state, { payload: { id } }) => {
    return state
      .updateIn(['systems'], systems => systems.filter(s => s.get('id') !== id))
  },

  // User actions
  [LOAD_USERS_REQUEST.SUCCESS]: mergeEntities
})
