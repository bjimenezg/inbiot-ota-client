//IMPORTANDO  LIBRERIAS...//
import { combineReducers } from 'redux' //devuelve un objeto cuyos valores son diferentes funciones reductoras en una única función reductora que puedes enviar a createStore
import { routerReducer } from 'react-router-redux'
import { reducer as formReducer } from 'redux-form'
//IMPORTANDO ARCHIVOS//
import entities from './entities'
import user from './user'
import modal from './modal'
import snackbar from './snackbar'
import loader from './loader'

import { LOGOUT } from 'common/actions/auth' //importar la accion de logout

export const makeRootReducer = (asyncReducers = {}) => {
  const appReducer = combineReducers({
    // add app reducers here
    entities,
    user,
    modal,
    snackbar,
    loader,

    // 3th party
    routing: routerReducer,
    form: formReducer,

    ...asyncReducers
  })

  return (state, action) => {
    if (action.type === LOGOUT) {
      const { routing } = state

      state = { routing }
    }

    return appReducer(state, action)
  }
}

export const injectReducer = (store, { key, reducer }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) {
    return
  }

  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
