import { takeLatest, call, put, all } from 'redux-saga/effects'
import { normalize } from 'normalizr'
import { push as pushHistory } from 'react-router-redux'

import actions, { constants } from 'common/actions/auth'
import snackbarActions from 'common/actions/snackbar'

import * as schemas from 'schemas'
import * as api from 'api/auth'
import * as Strings from 'constants/Strings'
import * as Routes from 'constants/Routes'
import { setTokenHeader, clearTokenHeader } from 'utils/request'
import { getAuthStoreData, setAuthStoreData, clearAuthStoreData } from 'utils/authStore'

export function * onAuthUser () {
  const authStoreData = getAuthStoreData()

  if (!authStoreData) {
    yield put(actions.authUserRequest.failure())
    return yield put(pushHistory(Routes.LOGIN))
  }

  try {
    yield call(setTokenHeader, authStoreData.token)
    const { user } = yield call(api.loadUser)
    const norm = yield call(normalize, user, schemas.user)

    yield put(actions.authUserRequest.success(norm))
    yield put(pushHistory(Routes.DASHBOARD))
  } catch (err) {
    if (__DEV__) console.log({ err })

    yield put(actions.authUserRequest.failure(err))
    yield put(pushHistory(Routes.LOGIN))
    yield put(snackbarActions.showSnackbar(Strings.EXPIRED_TOKEN))
  }
}

export function * onLogin ({ payload: data }) {
  yield put(actions.loginRequest.start())

  try {
    const { token, user } = yield call(api.login, data)

    yield call(setTokenHeader, token)
    yield call(setAuthStoreData, token)

    const norm = yield call(normalize, user, schemas.user)

    yield put(actions.loginRequest.success(norm))
    yield put(pushHistory(Routes.DASHBOARD))
  } catch (err) {
    if (__DEV__) console.log({ err })

    yield put(actions.loginRequest.failure(err))
  }
}

export function * onLogout () {
  yield call(clearAuthStoreData)
  yield call(clearTokenHeader)
  yield put(pushHistory(Routes.LOGIN))
}

export default function * watchAuth () {
  yield all([
    takeLatest(constants.AUTH_USER, onAuthUser),
    takeLatest(constants.LOGIN, onLogin),
    takeLatest(constants.LOGOUT, onLogout)
  ])
}
