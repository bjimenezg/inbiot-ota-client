import { takeLatest, call, put, all } from 'redux-saga/effects'
import { normalize } from 'normalizr'

import actions, { constants } from 'common/actions/system'
import snackbarActions from 'common/actions/snackbar'

import * as schemas from 'schemas'
import * as api from 'api/system'
import * as Strings from 'constants/Strings'

export function * onLoadSystems () {
  yield put(actions.loadSystemsRequest.start())

  try {
    const { systems } = yield call(api.loadSystems)
    const norm = yield call(normalize, systems, [schemas.system])

    yield put(actions.loadSystemsRequest.success(norm))
  } catch (err) {
    if (__DEV__) console.log({ err })

    yield put(actions.loadSystemsRequest.failure(err))
    yield put(snackbarActions.showSnackbar(Strings.WHOOPS))
  }
}

export function * onCreateSystem ({ payload: data }) {
  yield put(actions.createSystemRequest.start())

  try {
    const { system } = yield call(api.createSystem, data)
    const norm = yield call(normalize, system, schemas.system)

    yield put(actions.createSystemRequest.success(norm))
  } catch (err) {
    if (__DEV__) console.log({ err })

    yield put(actions.createSystemRequest.failure(err))
    yield put(snackbarActions.showSnackbar(Strings.WHOOPS))
  }
}

export function * onUpdateSystem ({ payload: data }) {
  yield put(actions.updateSystemRequest.start())

  try {
    const { system } = yield call(api.updateSystem, data)
    const norm = yield call(normalize, system, schemas.system)

    yield put(actions.updateSystemRequest.success(norm))
  } catch (err) {
    if (__DEV__) console.log({ err })

    yield put(actions.updateSystemRequest.failure(err))
    yield put(snackbarActions.showSnackbar(Strings.WHOOPS))
  }
}

export function * onDeleteSystem ({ payload: id }) {
  yield put(actions.deleteSystemRequest.start())

  try {
    yield call(api.deleteSystem, id)

    yield put(actions.deleteSystemRequest.success({ id }))
  } catch (err) {
    if (__DEV__) console.log({ err })

    yield put(actions.deleteSystemRequest.failure(err))
    yield put(snackbarActions.showSnackbar(Strings.WHOOPS))
  }
}
export default function * watchSystem () {
  yield all([
    takeLatest(constants.LOAD_SYSTEMS, onLoadSystems),
    takeLatest(constants.CREATE_SYSTEM, onCreateSystem),
    takeLatest(constants.UPDATE_SYSTEM, onUpdateSystem),
    takeLatest(constants.DELETE_SYSTEM, onDeleteSystem)
  ])
}
