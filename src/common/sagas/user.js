import { takeLatest, call, put, all } from 'redux-saga/effects'
import { normalize } from 'normalizr'

import actions, { constants } from 'common/actions/user'
import snackbarActions from 'common/actions/snackbar'

import * as schemas from 'schemas'
import * as api from 'api/user'
import * as Strings from 'constants/Strings'

export function * onLoadUsers () {
  yield put(actions.loadUsersRequest.start())

  try {
    const { users } = yield call(api.loadUsers)
    const norm = yield call(normalize, users, [schemas.user])

    yield put(actions.loadUsersRequest.success(norm))
  } catch (err) {
    if (__DEV__) console.log({ err })

    yield put(actions.loadUsersRequest.failure(err))
    yield put(snackbarActions.showSnackbar(Strings.WHOOPS))
  }
}

export default function * watchUser () {
  yield all([
    takeLatest(constants.LOAD_USERS, onLoadUsers)
  ])
}
