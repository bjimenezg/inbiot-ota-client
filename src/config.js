export const API_URL = __PROD__ ? 'https://inbiot-ota.herokuapp.com' : 'http://localhost:8080'
export const AUTH_STORE_NAMESPACE = 'sg-inbiot-ota-token'
