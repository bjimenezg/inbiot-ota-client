//LIBRERIAS//
import 'babel-polyfill' //Para adaptarse a las version ecma de los navegadores
import React from 'react'
import ReactDOM from 'react-dom'
import { browserHistory } from 'react-router' //es lo que lee de la url introducida en el navegador
import { syncHistoryWithStore } from 'react-router-redux'
//ARCHIVOS//
import { authUser } from 'common/actions/auth' //crea las acciones de auth (AUTH_USER, LOGIN, LOGOUT, AUTH_USER_REQUEST, LOGIN_REQUEST )
import configureStore from 'utils/configureStore'
import makeRootSaga from 'common/sagas' //utilizamos el middleware en redux
import Root from 'common/components/Root'

const store = configureStore({}, browserHistory)
const history = syncHistoryWithStore(browserHistory, store)
store.runSaga(makeRootSaga())
store.dispatch(authUser())

export const rootElement = document.getElementById('root')

const render = () => {
  ReactDOM.render(
    <Root
      store={store}
      history={history} />,
    rootElement
  )
}

if (module.hot) { //para que se recargue solo ante cambios (en desarrollo)
  module.hot.accept(['common/components/Root'], () => {
    ReactDOM.unmountComponentAtNode(rootElement)
    render()
  })
}

render()

export { store }
