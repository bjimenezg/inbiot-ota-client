import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import withStyles from '@material-ui/core/styles/withStyles'

import Avatar from '@material-ui/core/Avatar'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import LockIcon from '@material-ui/icons/LockOutlined'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'

import { login } from 'common/actions/auth'
import { getIsSubmitting, getError } from '../selectors'

const styles = (theme) => ({
  layout: {
    width: 'auto',
    display: 'block', // Fix IE11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto'
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE11 issue.
    marginTop: theme.spacing.unit
  },
  submit: {
    marginTop: theme.spacing.unit * 3
  },
  error: {
    color: 'red'
  }
})

const initialState = { //para el estado inicial declaramos el email y la password vacios
  email: '',
  password: ''
}

export class LoginContainer extends PureComponent {

  static propTypes = {
    classes: PropTypes.object.isRequired,
    submitError: PropTypes.any,
    isSubmitting: PropTypes.bool,
    login: PropTypes.func.isRequired
  }

  state = initialState //el del email y pass vacios

  handleInputValueChange = (event) => { //controlador de los cambios en los imput (email y contraseña)
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  handleSubmit = (event) => { // para cuando enviamos el formulario
    event.preventDefault() //para que no se envie el formuulario al servidor y no recargue la pagina web

    if (this.props.isSubmitting) return

    this.props.login({ ...this.state })
  }

  get error () { //posibles errores en el login
    const { classes, submitError } = this.props

    if (!submitError) { //si no hay ningun error al enviar el formuulario
      return null
    }

    let message = 'Unknown error. Please try again later.'

    if (submitError.res && submitError.res.status === 404) { //si ocurre un errror de authenticacion
      message = 'Wrong email or password'
    }

    return (
      <Typography variant='body1' className={classes.error}>
        {message}
      </Typography>
    )
  }

  render () {
    const { classes } = this.props
    const { email, password } = this.state

    return (
      <main className={classes.layout}>
        <Paper className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockIcon />
          </Avatar>
          <Typography variant='headline'>Sign in</Typography>
          <form className={classes.form} onSubmit={this.handleSubmit} noValidate>
            <TextField
              label='Email address'
              name='email'
              margin='normal'
              value={email}
              fullWidth
              required
              onChange={this.handleInputValueChange}
            />

            <TextField
              label='Password'
              name='password'
              type='password'
              margin='normal'
              value={password}
              fullWidth
              required
              onChange={this.handleInputValueChange}
            />

            {this.error} //si ocurre un error que se muestre entre el email y la password

            <Button
              type='submit'
              fullWidth
              variant='contained'
              color='primary'
              className={classes.submit}
            >
              Login
            </Button>
          </form>
        </Paper>
      </main>
    )
  }
}

const mapStateToProps = (state) => ({
  submitError: getError(state),
  isSubmitting: getIsSubmitting(state)
})

const mapActionsToProps = {
  login
}

export default connect(
  mapStateToProps,
  mapActionsToProps
)(
  withStyles(styles)(LoginContainer)
)
