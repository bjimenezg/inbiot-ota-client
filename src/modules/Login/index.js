import * as Routes from 'constants/Routes'
import { injectReducer } from 'common/reducers'

export default (store) => ({
  path: Routes.LOGIN,
  getComponent: async function (nextState, cb) {
    const [module, reducer] = await Promise.all([
      import('./containers/LoginContainer'),
      import('./reducers')
    ])

    injectReducer(store, { key: 'login', reducer: reducer.default })

    cb(null, module.default)
  }
})
