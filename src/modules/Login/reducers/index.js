import { createReducer } from 'utils'
import { fromJS } from 'immutable'

import { LOGIN_REQUEST } from 'common/actions/auth'

export const initialState = fromJS({
  isSubmitting: false,
  error: null
})

export default createReducer(initialState, {
  [LOGIN_REQUEST.START]: (state) => state.set('isSubmitting', true),
  [LOGIN_REQUEST.SUCCESS]: () => initialState,
  [LOGIN_REQUEST.FAILURE]: (state, { payload: error }) => {
    return state
      .set('isSubmitting', false)
      .set('error', error)
  }
})
