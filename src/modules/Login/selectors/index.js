export const getError = (state) => state.login.get('error')
export const getIsSubmitting = (state) => state.login.get('isSubmitting')
