import { schema } from 'normalizr'

export const measurement = new schema.Entity('measurements')

export const data = new schema.Entity('data', {
  measurements: [measurement]
})

export const system = new schema.Entity('systems', {
  data: [data]
})

export const user = new schema.Entity('users', {
  systems: [system]
})
