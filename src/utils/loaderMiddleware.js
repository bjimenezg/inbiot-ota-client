import _ from 'lodash'
import { toggleLoader } from 'common/actions/loader'

import {
  AUTH_USER_REQUEST,
  LOGIN_REQUEST
} from 'common/actions/auth'

import {
  LOAD_SYSTEMS_REQUEST,
  CREATE_SYSTEM_REQUEST,
  UPDATE_SYSTEM_REQUEST,
  DELETE_SYSTEM_REQUEST
} from 'common/actions/system'

const showLoaderActions = []
const hideLoaderActions = []

const addAsyncActions = actionTypes => {
  actionTypes.forEach((actionType) => {
    showLoaderActions.push(actionType.START)
    hideLoaderActions.push(actionType.SUCCESS, actionType.FAILURE)
  })
}

// Add actions here...
addAsyncActions([
  // User
  AUTH_USER_REQUEST,
  LOGIN_REQUEST,

  // Systems
  LOAD_SYSTEMS_REQUEST,
  CREATE_SYSTEM_REQUEST,
  UPDATE_SYSTEM_REQUEST,
  DELETE_SYSTEM_REQUEST
])

export default () => (dispatch) => (action) => {
  const type = action.type

  if (_.includes(showLoaderActions, type)) {
    dispatch(toggleLoader(true))
  }

  if (_.includes(hideLoaderActions, type)) {
    dispatch(toggleLoader(false))
  }

  return dispatch(action)
}
