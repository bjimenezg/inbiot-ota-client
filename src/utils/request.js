import { API_URL } from 'config'

let defaultHeaders = {
  accept: 'application/json',
  contentType: 'application/json'
}

export function setTokenHeader (value) {
  defaultHeaders['token'] = value
}

export function clearTokenHeader () {
  delete defaultHeaders['token']
}

export default function request (path, args = {}) {
  const url = `${API_URL}/${path}`

  const options = {
    headers: defaultHeaders,
    method: args.method || 'GET'
  }

  if (args.data) {
    options.body = JSON.stringify(args.data)
    options.headers['content-type'] = 'application/json'
  }

  return fetch(url, options)
    .then((res) => {
      if (res.status >= 200 && res.status < 300) {
        return res
      }

      const error = new Error(res.statusText)
      error.res = res
      throw error
    })
    .then((res) => {
      if (res.status === 204) {
        return res
      }

      return res.json().then((body) => [body, res])
    })
}
